# Cassandra plugin for BEANS

`BEANS` is a web-based software for interactive distributed data analysis with a clear interface for querying, filtering, aggregating, and plotting data from an arbitrary number of datasets and tables [https://gitlab.com/beans-code/beans](https://gitlab.com/beans-code/beans).

`Cassandra` is an Open Source NoSQL Database which allow to manage massive amounts of data [https://cassandra.apache.org/_/index.html](https://cassandra.apache.org/_/index.html).

This plugin allow to store `BEANS` database in `Cassandra`.
