package net.beanscode.plugin.cassandra;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.connectors.CassandraConnector;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.plugins.Func;
import net.beanscode.model.plugins.Plugin;
import net.hypki.libs5.db.cassandra.CassandraProvider;
import net.hypki.libs5.db.db.DatabaseProvider;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchEngineProvider;

import org.rendersnake.HtmlCanvas;

public class CassandraDbPlugin implements Plugin {
	
	public CassandraDbPlugin() {
		
	}

	@Override
	public String getName() {
		return "Cassandra database provider";
	}

	@Override
	public String getVersion() {
		return "1.0.0";
	}

	@Override
	public String getDescription() {
		return "Cassandra database provider allows to store BEANS data on distributed database Apache Cassandra "
				+ "(suitable for cases when the data takes many terabytes of disks space)";
	}

	@Override
	public String getPanel(UUID userId) throws IOException {
		return new HtmlCanvas()
			.h3()
				.content("Description")
			.p()
				.content(getDescription()).toHtml();
	}

	@Override
	public List<Func> getFuncList() {
		return null;
	}

	@Override
	public List<Class<? extends Connector>> getConnectorClasses() {
		List<Class<? extends Connector>> conn = new ArrayList<Class<? extends Connector>>();
		conn.add(CassandraConnector.class);
		return conn;
	}

	@Override
	public List<Class<? extends NotebookEntry>> getNotebookEntries() {
		return null;
	}

	@Override
	public List<Class<? extends DatabaseProvider>> getDatabaseProviderList() {
		List<Class<? extends DatabaseProvider>> db = new ArrayList<>();
		db.add(CassandraProvider.class);
		return db;
	}
	
	@Override
	public List<Class<? extends SearchEngineProvider>> getSearchEngineProviderList() {
		return null;
	}
	
	@Override
	public boolean selfTest(UUID userId, OutputStreamWriter output) throws ValidationException, IOException {
		output.write("TODO");
		return true;
	}
}
